const Channel = require('../../models/messaging/channel')

const check_if_channel_exists = (message) => {
  const p = new Promise((res, rej) => {
    Channel.findOne({'channel_id': message.channel_id}, (err, channel) => {
      if(err){
        rej(err)
      }
      if(!channel){
        create_new_channel(message).then((channel) => {
          res(channel)
        }).catch((err) => {
          rej(err)
        })
      }else{
        console.log(channel)
        rej("Channel already exists, no further action necessary")
      }
    })
  })
  return p
}

const create_new_channel = (message) => {
  const p = new Promise((res, rej) => {
    const c = {
      channel_id: message.channel_id,
      building_id: message.building_id,
      participants: [message.sender_id, message.receiver_id]
    }
    const new_channel = new Channel(c)
    new_channel.save((err, channel) => {
      if(err){
        rej(err)
      }
      res(channel)
    })
  })
  return p
}

module.exports = check_if_channel_exists
