const Channel = require('../../models/messaging/channel')

const get_channels = (userId) => {
  const p = new Promise((res, rej) => {
    Channel.find({'participants': userId}, (err, channels) => {
      if(err){
        rej(err)
      }
      res(channels)
    })
  })
  return p
}

module.exports = get_channels
