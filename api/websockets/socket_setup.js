const socketio = require('socket.io');
const Rx = require('rxjs')
const get_channels = require('../channels/get_channel')
const save_message = require('../messaging/save_message')
const send_message = require('../messaging/send_message')
const check_if_channel_exists = require('../channels/create_channel')

// Setup websockets
const SocketCommunications = (server) => {
  // websocket is set up
  const io = socketio(server)

  // Obv for handling establishment of connections
  Rx.Observable.create((observer) => {
    // 0. instantiate connection
    io.on('connection', (socket) => {
        console.log('===================================')
        console.log(`0) Connection with Socket# ${socket.id} has been established`)
        // 1. send client the unique socketID
        console.log(`1) verifying client with Socket# ${socket.id} in server`)
        socket.emit('step1: identify client', {'socketId': socket.id, 'connectTime': Date.now()})
        // 2. await for client to verify receival of unique socketID
        socket.on('step2: client verified', ({ data, userId }) => {
            console.log(`2) verified client with Socket# ${socket.id} in server`)
            console.log(`3) completing full handshake cycle with Socket# ${socket.id}`)
            get_channels(userId).then((channels) => {
              // 3. completing full handshake cycle
              observer.next({
                socket,
                data,
                channels
              })
            }).catch((err) => {
              observer.error(err)
            })
        })
    })
    return function() {
        io.close()
    }
  }).subscribe({
    next: ({socket, data, channels})=>{
      socket.emit('step3: handshake complete', {
        data,
        channels
      })
    },
    error: (err) => {
      console.log(err)
    },
    complete: () => {}
  })

  // Obv for handling end of connections
  Rx.Observable.create((observer) => {
      // 1. hook onto the instantiated connection
      io.on('connection', (socket) => {
          // 2. determine what happens on disconnect of this socket
          socket.on('disconnect', () => {
              observer.next({
                socket,
                'event': 'client disconnect'
              })
          })
      })
      return function() {
          io.close()
      }
  }).subscribe({
    next: ({socket})=>{
      console.log('===================================')
      console.log(`Socket # ${socket.id} has disconnected`)
    },
    error: () => {},
    complete: () => {}
  })

  // Obv for handling messages
  Rx.Observable.create((observer) => {
    io.on('connection', (socket) => {
      // there is no unsubscribe_channel
      socket.on('subscribe_channel', (channel_id) => {
        console.log('joining channel: ', channel_id);
        socket.join(channel_id);
        socket.on('message_from_client', (message) => {
          // message = {
          //   ...message,
          //   channel_id
          // }
          if(message.channel_id === channel_id){
            observer.next({
              socket,
              message
            })
          }
        })
        socket.on('disconnect', () => {
            console.log('leaving channel: ', channel_id);
            socket.leave(channel_id)
        })
      })

      socket.on('message_from_client', (message) => {
        // message = {
        //   ...message,
        //   channel_id
        // }
        if(!message.channel_exists){
          check_if_channel_exists(message).then((channel) => {
            console.log('new_channel created')
            socket.emit('new_channel', channel)
            socket.join(channel.channel_id);
            observer.next({
              socket,
              message
            })
          }).catch((err) => {
            console.log(err)
          })
        }
      })
    })
  }).subscribe({
    next: ({ socket, message }) => {
      send_message(socket, message)
      save_message(message)
    }
  })
}

module.exports = SocketCommunications
