// Import dependencies on Mongoose
const mongoose = require('mongoose');

// Create the Rental post Schema
const ChannelSchema = new mongoose.Schema({
  channel_id: String,
  building_id: Number,
  participants: [Number],
})

ChannelSchema.pre('save', function(next){
	this.last_active = new Date().getTime() / 1000
	next();
})

const Channel = mongoose.model('Channel', ChannelSchema);

module.exports = Channel;
