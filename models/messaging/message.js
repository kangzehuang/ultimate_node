// Import dependencies on Mongoose
const mongoose = require('mongoose');

// Create the Rental post Schema
const MessageSchema = new mongoose.Schema({
	message_id: String,
	sender_id: Number,
  sender_name: String,
  receiver_id: Number,
  building_id: Number,
  channel_id: String,
  contents: String,
	seen_at: Number,
	sent_at: Number
});

MessageSchema.pre('save', function(next){
	this.sent_at = new Date().getTime() / 1000
	this.seen_at = null
	this.channel_id = `${this.sender_id}_${this.receiver_id}_${this.building_id}`
	next();
});

const Message = mongoose.model('Message', MessageSchema);

module.exports = Message;
